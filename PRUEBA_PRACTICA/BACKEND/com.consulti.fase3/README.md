##PASOS PARA LA EJECUCION DEL BACKEND(DOCKER)
##### 1. Crear una red en docker para correr nuestra aplicacion
docker network create red-consulti
##### 2. Pull de la imagen MySQL version 8 del repositorio de Docker
docker pull mysql:8
##### 3. Correr la imagen MySQL en un contenedor llamado "basededatos" en la red creada previamente
docker run -d -p 33061:3306 --name basededatos --network red-consulti -e MYSQL_ROOT_PASSWORD=m1ch43lhidalgo -e MYSQL_DATABASE=fase3 mysql:8 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
##### 4. Revisar que el archivo de configuracion envez de estar localhost:PORT este el nombre del contenedor que almacena la base de datos en Docker
spring.datasource.url=jdbc:mysql://basededatos/fase3?allowPublicKeyRetrieval=true&useSSL=false
##### 5. Crear una imagen de este springboot(EN EL CASO DE HACER CAMBIOS AL ARCHIVO NO OLVIDAR GENERAR EL .JAR ANTES DE CREAR LA IMAGEN CON EL CAMANDO mvn package)
docker build -t springboot .
##### 6. Correr la imagen de Springboot en la misma red
docker run -p 8082:8080 --name backend --network red-consulti springboot
##### 7. Ahora para correr el frontend seguir las instrucciones del archivo README.md de la carpeta prueba-fase-3