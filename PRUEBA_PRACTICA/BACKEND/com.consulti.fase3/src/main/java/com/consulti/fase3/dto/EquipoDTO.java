package com.consulti.fase3.dto;

import java.util.ArrayList;
import java.util.List;

import com.consulti.fase3.model.Usuario;

public class EquipoDTO {
    private Long Id;
    private String nombres;
    private String identC;
    private int equipo;
    private int area;
    private int cargo;
	private List<Usuario> miembros = new ArrayList<>();
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getIdentC() {
        return identC;
    }
    public void setIdentC(String identC) {
        this.identC = identC;
    }
    public int getEquipo() {
        return equipo;
    }
    public void setEquipo(int equipo) {
        this.equipo = equipo;
    }
    public int getArea() {
        return area;
    }
    public void setArea(int area) {
        this.area = area;
    }
    public int getCargo() {
        return cargo;
    }
    public void setCargo(int cargo) {
        this.cargo = cargo;
    }
    public List<Usuario> getMiembros() {
        return miembros;
    }
    public void setMiembros(List<Usuario> miembros) {
        this.miembros = miembros;
    }
    public EquipoDTO() {
    }
    public EquipoDTO(Long id, String nombres, String identC, int equipo, int area, int cargo, List<Usuario> miembros) {
        Id = id;
        this.nombres = nombres;
        this.identC = identC;
        this.equipo = equipo;
        this.area = area;
        this.cargo = cargo;
        this.miembros = miembros;
    }
    @Override
    public String toString() {
        return "EquipoDTO [Id=" + Id + ", nombres=" + nombres + ", identC=" + identC + ", equipo=" + equipo + ", area="
                + area + ", cargo=" + cargo + ", miembros=" + miembros + "]";
    }
    
    
}
