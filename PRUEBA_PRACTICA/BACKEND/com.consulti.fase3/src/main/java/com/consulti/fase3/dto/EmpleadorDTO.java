package com.consulti.fase3.dto;

public class EmpleadorDTO {
    private Long Id;
    private String nombres;
    private String apellidos;
    private String email;
    private String password;
    private String fecha_nacimiento;
    private int area;
    private String identificador;
    /**
     * Generar los Getters y Setters
     * @return
     */
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }
    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }
    public int getArea() {
        return area;
    }
    public void setArea(int area) {
        this.area = area;
    }
    public String getIdentificador() {
        return identificador;
    }
    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }
    /**
     * Generar constructor vacio
     * 
     */
    public EmpleadorDTO() {
    }
    /**
     * Generar constructor con los atributos
     * @param id
     * @param nombres
     * @param apellidos
     * @param email
     * @param password
     * @param fecha_nacimiento
     * @param area
     * @param identificador
     */
    public EmpleadorDTO(Long id, String nombres, String apellidos, String email, String password,
            String fecha_nacimiento, int area, String identificador) {
        Id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.password = password;
        this.fecha_nacimiento = fecha_nacimiento;
        this.area = area;
        this.identificador = identificador;
    }
    /** 
     * Generar ToString
     * 
     */
    @Override
    public String toString() {
        return "EmpleadorDTO [Id=" + Id + ", nombres=" + nombres + ", apellidos=" + apellidos + ", email=" + email
                + ", password=" + password + ", fecha_nacimiento=" + fecha_nacimiento + ", area=" + area
                + ", identificador=" + identificador + "]";
    }
    
    
}
