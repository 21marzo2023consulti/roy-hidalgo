package com.consulti.fase3.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tarea")
public class Tarea {
    private static final Long serialVersionUID = 1L;
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    private String nombre;
    private int area;
    private int time;
    private int inicio;
    private int fin;
    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "lider_id")
    @JsonIgnoreProperties ({"hibernateLazyInitializer","handler"})
    private Usuario lider;
    private String descripcion;
    public static Long getSerialversionuid() {
        return serialVersionUID;
    }
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getArea() {
        return area;
    }
    public void setArea(int area) {
        this.area = area;
    }
    public int getTime() {
        return time;
    }
    public void setTime(int time) {
        this.time = time;
    }
    public int getInicio() {
        return inicio;
    }
    public void setInicio(int inicio) {
        this.inicio = inicio;
    }
    public int getFin() {
        return fin;
    }
    public void setFin(int fin) {
        this.fin = fin;
    }
    public Usuario getLider() {
        return lider;
    }
    public void setLider(Usuario lider) {
        this.lider = lider;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public Tarea() {
    }
    public Tarea(Long id, String nombre, int area, int time, int inicio, int fin, Usuario lider, String descripcion) {
        Id = id;
        this.nombre = nombre;
        this.area = area;
        this.time = time;
        this.inicio = inicio;
        this.fin = fin;
        this.lider = lider;
        this.descripcion = descripcion;
    }
    @Override
    public String toString() {
        return "Tarea [Id=" + Id + ", nombre=" + nombre + ", area=" + area + ", time=" + time + ", inicio=" + inicio
                + ", fin=" + fin + ", lider=" + lider + ", descripcion=" + descripcion + "]";
    }
   
    

    
}
