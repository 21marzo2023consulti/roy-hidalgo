package com.consulti.fase3.response;

public class EquipoResponseRest extends ResponseRest{
  private EquipoResponse equipoResponse = new EquipoResponse();

public EquipoResponse getEquipoResponse() {
    return equipoResponse;
}

public void setEquipoResponse(EquipoResponse equipoResponse) {
    this.equipoResponse = equipoResponse;
}
  
}
