package com.consulti.fase3.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "asignaciones")
public class Asignaciones implements Serializable{
    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "coordinador_id")
    @JsonIgnoreProperties ({"hibernateLazyInitializer","handler"})
    private Usuario coordinador;
    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "perativos_id")
    @JsonIgnoreProperties ({"hibernateLazyInitializer","handler"})
    private Usuario operativos;
    public static Long getSerialversionuid() {
        return serialVersionUID;
    }
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public Usuario getCoordinador() {
        return coordinador;
    }
    public void setCoordinador(Usuario coordinador) {
        this.coordinador = coordinador;
    }
    public Usuario getOperativos() {
        return operativos;
    }
    public void setOperativos(Usuario operativos) {
        this.operativos = operativos;
    }
    public Asignaciones() {
    }
    public Asignaciones(Long id, Usuario coordinador, Usuario operativos) {
        Id = id;
        this.coordinador = coordinador;
        this.operativos = operativos;
    }
    @Override
    public String toString() {
        return "Asignaciones [Id=" + Id + ", coordinador=" + coordinador + ", operativos=" + operativos + "]";
    }
   
    
   
  
    
}
