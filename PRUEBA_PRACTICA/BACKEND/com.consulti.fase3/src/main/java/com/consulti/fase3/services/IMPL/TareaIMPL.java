package com.consulti.fase3.services.IMPL;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.consulti.fase3.model.Tarea;
import com.consulti.fase3.model.Usuario;
import com.consulti.fase3.repo.TareaRepo;
import com.consulti.fase3.repo.UsuarioRepo;
import com.consulti.fase3.response.TareaResponseRest;
import com.consulti.fase3.services.TareaService;

import jakarta.transaction.Transactional;

@Service
public class TareaIMPL implements TareaService {
  
    @Autowired
    private TareaRepo tareaRepo;
  
    @Autowired
    private UsuarioRepo usuarioRepo;
    @Override
    @Transactional
    public ResponseEntity<TareaResponseRest> search() {
        TareaResponseRest response = new TareaResponseRest();
		try {
			List<Tarea> tarea = (List<Tarea>) tareaRepo.findAll();
			response.getTareaResponse().setTarea(tarea);
			response.setMetadata("Respuesta correcta", "00", "Respuesta exitosa");
		} catch (Exception e) {
			
			response.setMetadata("Respuesta incorrecta", "-1", "Error al consultar");
			e.getStackTrace();
			return new ResponseEntity<TareaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<TareaResponseRest>(response,HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<TareaResponseRest> searchById(Long id) {
        TareaResponseRest response = new TareaResponseRest();
		List<Tarea> list = new ArrayList<>();
		try {
			Optional<Tarea> data = tareaRepo.findById(id);
			if(data.isPresent()) {
				list.add(data.get());
				response.getTareaResponse().setTarea(list);
				response.setMetadata("Respuesta correcta", "00", "Usuario encontrado");
			} else {
				response.setMetadata("Respuesta incorrecta", "-1", "Usuario no encontrado");
				return new ResponseEntity<TareaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e){
			response.setMetadata("Respuesta incorrecta", "-1", "Error al consultar por id");
			e.getStackTrace();
			return new ResponseEntity<TareaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<TareaResponseRest>(response,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<TareaResponseRest> save(Tarea tarea, Long idLider) {
        TareaResponseRest response = new TareaResponseRest();

        
		List<Tarea> list = new ArrayList<>();
		try {
			Optional<Usuario> lider = usuarioRepo.findById(idLider);
			tarea.setArea(tarea.getArea());
			tarea.setNombre(tarea.getNombre());
			tarea.setDescripcion(tarea.getDescripcion());
			tarea.setInicio(tarea.getInicio());
			tarea.setFin(tarea.getFin());
			tarea.setLider(lider.get());
			Tarea dataSaved = tareaRepo.save(tarea);
			if(dataSaved != null) {
				list.add(dataSaved);
				response.getTareaResponse().setTarea(list);
				response.setMetadata("Respuesta correcta", "00", "Data guardada");
			} else {
				response.setMetadata("Respuesta icorrecta", "-1", "Data no guardado");
				return new ResponseEntity<TareaResponseRest>(response,HttpStatus.BAD_REQUEST);
			}
			
		}
		 catch (Exception e){
				response.setMetadata("Respuesta incorrecta", "-1", "Error al grabar uuario");
				e.getStackTrace();
				return new ResponseEntity<TareaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<TareaResponseRest>(response,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<TareaResponseRest> update(Tarea tarea, Long id) {
       TareaResponseRest response = new TareaResponseRest();
		List<Tarea> list = new ArrayList<>();
		try {
			Optional<Tarea> dataSearch = tareaRepo.findById(id);
			if(dataSearch.isPresent()) {
				//Save the register
				dataSearch.get().setInicio(tarea.getInicio());
				dataSearch.get().setFin(tarea.getFin());
			
				Tarea dataToUpdate = tareaRepo.save(dataSearch.get());
				if(dataToUpdate != null) {
					list.add(dataToUpdate);
					response.getTareaResponse().setTarea(list);
					response.setMetadata("Respuesta correcta", "00", "Usuario actualizado");
					
				}
				
				
			} else {
				response.setMetadata("Respuesta incorrecta", "-1", " Usuario no actualizado");
				return new ResponseEntity<TareaResponseRest>(response, HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e){
			response.setMetadata("Respuesta incorrecta", "-1", "Error al grabar usuario");
			e.getStackTrace();
			return new ResponseEntity<TareaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<TareaResponseRest>(response,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<TareaResponseRest> deleteById(Long id) {
        TareaResponseRest response = new TareaResponseRest();
		try {
			tareaRepo.deleteById(id);
			response.setMetadata("Respuesta correcta", "00", "Registro eliminado");
		}catch (Exception e) {
			response.setMetadata("Respuesta incorecta", "-1","Error al eliminar");
			e.getStackTrace();
			return new ResponseEntity<TareaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<TareaResponseRest>(response,HttpStatus.OK);
    }
        
}
