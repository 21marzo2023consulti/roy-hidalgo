package com.consulti.fase3.response;

import java.util.List;

import com.consulti.fase3.model.Empleado;

public class EmpleadoResponse {
    private List<Empleado> empleado;

    public List<Empleado> getEmpleado() {
        return empleado;
    }

    public void setEmpleado(List<Empleado> empleado) {
        this.empleado = empleado;
    }
    
    
}
