package com.consulti.fase3.services.IMPL;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.consulti.fase3.model.Empleado;
import com.consulti.fase3.repo.EmpleadoRepo;
import com.consulti.fase3.response.EmpleadoResponseRest;
import com.consulti.fase3.services.EmpleadoService;

import jakarta.transaction.Transactional;
@Service
public class EmpleadoIMPL implements EmpleadoService {

    @Autowired
    private EmpleadoRepo empleadoRepo;
    @Override
    public ResponseEntity<EmpleadoResponseRest> search() {
		EmpleadoResponseRest response = new EmpleadoResponseRest();
		try {
			List<Empleado> empleado = (List<Empleado>) empleadoRepo.findAll();
			response.getEmpleadoResponse().setEmpleado(empleado);
			response.setMetadata("Respuesta correcta", "00", "Respuesta exitosa");
		} catch (Exception e) {
			
			response.setMetadata("Respuesta incorrecta", "-1", "Error al consultar");
			e.getStackTrace();
			return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.OK);
	}

    @Override
    @Transactional
    public ResponseEntity<EmpleadoResponseRest> searchById(Long id) {
        EmpleadoResponseRest response = new EmpleadoResponseRest();
		List<Empleado> list = new ArrayList<>();
		try {
			Optional<Empleado> data = empleadoRepo.findById(id);
			if(data.isPresent()) {
				list.add(data.get());
				response.getEmpleadoResponse().setEmpleado(list);
				response.setMetadata("Respuesta correcta", "00", "Usuario encontrado");
			} else {
				response.setMetadata("Respuesta incorrecta", "-1", "Usuario no encontrado");
				return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e){
			response.setMetadata("Respuesta incorrecta", "-1", "Error al consultar por id");
			e.getStackTrace();
			return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<EmpleadoResponseRest> save(Empleado empleado) {
        EmpleadoResponseRest response = new EmpleadoResponseRest();
		   /**
         * Generar el identificador
         */
        String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String numeros = "0123456789";
        StringBuilder cadenaAleatoria = new StringBuilder();
        Random random = new Random();
        
        // Generar 2 letras aleatorias
        for (int i = 0; i < 2; i++) {
            int index = random.nextInt(letras.length());
            char letra = letras.charAt(index);
            cadenaAleatoria.append(letra);
        }
        
        // Generar 2 números aleatorios
        for (int i = 0; i < 2; i++) {
            int index = random.nextInt(numeros.length());
            char numero = numeros.charAt(index);
            cadenaAleatoria.append(numero);
        }
        
        String cadena = cadenaAleatoria.toString();
        
		List<Empleado> list = new ArrayList<>();
		try {
			Empleado dataSaved = empleadoRepo.save(empleado);
			if(dataSaved != null) {
				list.add(dataSaved);
				response.getEmpleadoResponse().setEmpleado(list);
				response.setMetadata("Respuesta correcta", "00", "Data guardada");
			} else {
				response.setMetadata("Respuesta icorrecta", "-1", "Data no guardado");
				return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.BAD_REQUEST);
			}
			
		}
		 catch (Exception e){
				response.setMetadata("Respuesta incorrecta", "-1", "Error al grabar uuario");
				e.getStackTrace();
				return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<EmpleadoResponseRest> update(Empleado empleado, Long id) {
        EmpleadoResponseRest response = new EmpleadoResponseRest();
		List<Empleado> list = new ArrayList<>();
		try {
			Optional<Empleado> dataSearch = empleadoRepo.findById(id);
			if(dataSearch.isPresent()) {
				//Save the register
				dataSearch.get().setEmail(empleado.getEmail());
				dataSearch.get().setFecha_nacimiento(empleado.getFecha_nacimiento());
				
				
				Empleado dataToUpdate = empleadoRepo.save(dataSearch.get());
				if(dataToUpdate != null) {
					list.add(dataToUpdate);
					response.getEmpleadoResponse().setEmpleado(list);
					response.setMetadata("Respuesta correcta", "00", "Usuario actualizado");
					
				}
				
				
			} else {
				response.setMetadata("Respuesta incorrecta", "-1", " Usuario no actualizado");
				return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e){
			response.setMetadata("Respuesta incorrecta", "-1", "Error al grabar usuario");
			e.getStackTrace();
			return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<EmpleadoResponseRest> deleteById(Long id) {
        EmpleadoResponseRest response = new EmpleadoResponseRest();
		try {
			empleadoRepo.deleteById(id);
			response.setMetadata("Respuesta correcta", "00", "Registro eliminado");
		}catch (Exception e) {
			response.setMetadata("Respuesta incorecta", "-1","Error al eliminar");
			e.getStackTrace();
			return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<EmpleadoResponseRest>(response,HttpStatus.OK);
    }
        
}
