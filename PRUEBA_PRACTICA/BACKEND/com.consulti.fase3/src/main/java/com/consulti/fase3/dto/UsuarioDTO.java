package com.consulti.fase3.dto;

import java.util.List;

import com.consulti.fase3.model.Asignaciones;

public class UsuarioDTO {
    private Long Id;
    private String nombres;
    private String apellidos;
    private String email;
    private String password;
    private String fecha_nacimiento;
    private int area;
    private String identificador;
    private int rol;
    private List<Asignaciones> asignaciones;
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }
    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }
    public int getArea() {
        return area;
    }
    public void setArea(int area) {
        this.area = area;
    }
    public String getIdentificador() {
        return identificador;
    }
    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }
    public int getRol() {
        return rol;
    }
    public void setRol(int rol) {
        this.rol = rol;
    }
    public List<Asignaciones> getAsignaciones() {
        return asignaciones;
    }
    public void setAsignaciones(List<Asignaciones> asignaciones) {
        this.asignaciones = asignaciones;
    }
    public UsuarioDTO() {
    }
    public UsuarioDTO(Long id, String nombres, String apellidos, String email, String password, String fecha_nacimiento,
            int area, String identificador, int rol, List<Asignaciones> asignaciones) {
        Id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.password = password;
        this.fecha_nacimiento = fecha_nacimiento;
        this.area = area;
        this.identificador = identificador;
        this.rol = rol;
        this.asignaciones = asignaciones;
    }
    @Override
    public String toString() {
        return "UsuarioDTO [Id=" + Id + ", nombres=" + nombres + ", apellidos=" + apellidos + ", email=" + email
                + ", password=" + password + ", fecha_nacimiento=" + fecha_nacimiento + ", area=" + area
                + ", identificador=" + identificador + ", rol=" + rol + ", asignaciones=" + asignaciones + "]";
    }

    


}
