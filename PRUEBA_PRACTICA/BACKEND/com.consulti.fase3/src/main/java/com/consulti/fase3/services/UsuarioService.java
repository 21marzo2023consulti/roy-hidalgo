package com.consulti.fase3.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.consulti.fase3.dto.LoginDTO;
import com.consulti.fase3.dto.UsuarioDTO;
import com.consulti.fase3.model.Usuario;
import com.consulti.fase3.response.LoginResponse;
import com.consulti.fase3.response.UsuarioResponseRest;

@Repository
public interface UsuarioService {
    public ResponseEntity<UsuarioResponseRest> search();
    String registerUsuario(UsuarioDTO usuarioDTO);
    LoginResponse loginUser(LoginDTO loginDTO);
    public ResponseEntity<UsuarioResponseRest> update(Usuario usuario, Long id);
    public ResponseEntity<UsuarioResponseRest> deleteById(Long id);
}
