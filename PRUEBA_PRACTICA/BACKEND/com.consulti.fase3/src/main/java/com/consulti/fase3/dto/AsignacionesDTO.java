package com.consulti.fase3.dto;

import java.util.ArrayList;
import java.util.List;

import com.consulti.fase3.model.Usuario;

public class AsignacionesDTO {
    private Long Id;
    private Usuario coordinador;
    private Usuario operativos;
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public Usuario getCoordinador() {
        return coordinador;
    }
    public void setCoordinador(Usuario coordinador) {
        this.coordinador = coordinador;
    }
    public Usuario getOperativos() {
        return operativos;
    }
    public void setOperativos(Usuario operativos) {
        this.operativos = operativos;
    }
    public AsignacionesDTO() {
    }
    public AsignacionesDTO(Long id, Usuario coordinador, Usuario operativos) {
        Id = id;
        this.coordinador = coordinador;
        this.operativos = operativos;
    }
    @Override
    public String toString() {
        return "AsignacionesDTO [Id=" + Id + ", coordinador=" + coordinador + ", operativos=" + operativos + "]";
    }
    
}
