package com.consulti.fase3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude=SecurityAutoConfiguration.class)
public class Prueba3DeConsultiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Prueba3DeConsultiApplication.class, args);
	}

}
