package com.consulti.fase3.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.consulti.fase3.model.Asignaciones;

@EnableJpaRepositories
@Repository
public interface AsignacionesRepo extends JpaRepository <Asignaciones, Long>{
    
}
