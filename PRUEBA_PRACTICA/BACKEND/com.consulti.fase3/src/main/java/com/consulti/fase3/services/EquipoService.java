package com.consulti.fase3.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.consulti.fase3.model.Equipo;
import com.consulti.fase3.response.EquipoResponseRest;
@Repository
public interface EquipoService {
    public ResponseEntity<EquipoResponseRest> search();
	public ResponseEntity<EquipoResponseRest> searchById(Long id);
	public ResponseEntity<EquipoResponseRest> save(Equipo equipo);
	public ResponseEntity<EquipoResponseRest> update(Equipo equipo, Long id);
	public ResponseEntity<EquipoResponseRest> deleteById(Long id);
}
