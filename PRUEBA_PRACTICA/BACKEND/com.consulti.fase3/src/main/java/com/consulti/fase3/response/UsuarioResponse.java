package com.consulti.fase3.response;

import java.util.List;

import com.consulti.fase3.model.Usuario;

public class UsuarioResponse {
    private List<Usuario> usuario;

    public List<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(List<Usuario> usuario) {
        this.usuario = usuario;
    }
    
}
