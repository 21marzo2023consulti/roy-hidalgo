package com.consulti.fase3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.consulti.fase3.dto.LoginDTO;
import com.consulti.fase3.dto.UsuarioDTO;
import com.consulti.fase3.model.Usuario;
import com.consulti.fase3.response.LoginResponse;
import com.consulti.fase3.response.UsuarioResponseRest;
import com.consulti.fase3.services.UsuarioService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/usuario")
public class UserController {
    /**
     * Inyecto el servicio
     */
    @Autowired
    private UsuarioService usuarioService;

    @PostMapping(path = "/register")
    public String registerUsuario(@RequestBody UsuarioDTO usuarioDTO){
        String id = usuarioService.registerUsuario(usuarioDTO);
        return id;
    }

     /**
     * Metodo para encontrar todos los usuarios
     */
    @GetMapping("/all")
	public ResponseEntity<UsuarioResponseRest> search(){
        ResponseEntity<UsuarioResponseRest> response = usuarioService.search();
        return response;
    }
    /**
     * Metodo para logear usuarios
     */
    @PostMapping(path = "/login")
    public ResponseEntity<?> loginUser(@RequestBody LoginDTO loginDTO){
		LoginResponse loginResponse = usuarioService.loginUser(loginDTO);
		return ResponseEntity.ok(loginResponse);
	}
 /**
     * Metodo para actualizar
     */
    @PutMapping("/{id}")
	public ResponseEntity<UsuarioResponseRest> update(@RequestBody Usuario promociones, @PathVariable Long id){
		 ResponseEntity<UsuarioResponseRest> response = usuarioService.update(promociones,id);
		 return response;
	}
    /**
     * Borrar al usuario
     */

    @DeleteMapping("/{id}")
	public ResponseEntity<UsuarioResponseRest> delete(@PathVariable Long id){
		 ResponseEntity<UsuarioResponseRest> response = usuarioService.deleteById(id);
		 return response;
	}
}
