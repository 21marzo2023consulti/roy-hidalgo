package com.consulti.fase3.dto;

public class LoginDTO {
    /**
     * Declaro los campos con los que voy a iniciar sesion para este ejercicio el email y el password
     */
    private String email;
    private String password;
    private int rol;
    /**
     * Genero los Getters y Setters 
     */
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public int getRol() {
        return rol;
    }
    public void setRol(int rol) {
        this.rol = rol;
    }
    /**
     * Generar constructor
     */
    public LoginDTO() {
    }
    /**
     * Generar constructor con sus atributos
     * @param email
     * @param password
     * @param rol
     */
    public LoginDTO(String email, String password, int rol) {
        this.email = email;
        this.password = password;
        this.rol = rol;
    }
    /**
     * Generar ToString
     */
    @Override
    public String toString() {
        return "LoginDTO [email=" + email + ", password=" + password + ", rol=" + rol + "]";
    }
    
}
