package com.consulti.fase3.response;

import java.util.List;

import com.consulti.fase3.model.Asignaciones;

public class AsignacionesResponse {
    private List<Asignaciones> asignaciones;

    public List<Asignaciones> getAsignaciones() {
        return asignaciones;
    }

    public void setAsignaciones(List<Asignaciones> asignaciones) {
        this.asignaciones = asignaciones;
    }
    
}
