package com.consulti.fase3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import com.consulti.fase3.model.Asignaciones;
import com.consulti.fase3.response.AsignacionesResponseRest;
import com.consulti.fase3.services.AsignacionesService;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1")
public class AsignacionesController {
    @Autowired
    private AsignacionesService asignacionesService;

    @PostMapping("/asignaciones")
	public ResponseEntity<AsignacionesResponseRest> save(@RequestParam("coordinador" ) Long idCoordinador, @RequestParam("operativos") Long idsOperativos) throws IOException{
		 Asignaciones asignacion = new Asignaciones();
			ResponseEntity<AsignacionesResponseRest> response = asignacionesService.save(asignacion,idCoordinador,idsOperativos);
			return response;
		 
	}
	//Metodo para encontrar todos las asignaciones
	    /**
     * Metodo para encontrar todos los usuarios
     */
    @GetMapping("/asignaciones/all")
	public ResponseEntity<AsignacionesResponseRest> search(){
        ResponseEntity<AsignacionesResponseRest> response = asignacionesService.search();
        return response;
    }

}
