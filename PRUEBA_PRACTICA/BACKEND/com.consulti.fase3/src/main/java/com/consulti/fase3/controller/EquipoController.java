package com.consulti.fase3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.consulti.fase3.model.Equipo;
import com.consulti.fase3.response.EquipoResponseRest;
import com.consulti.fase3.services.EquipoService;


@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1")
public class EquipoController {
    @Autowired
    private EquipoService equipoService;
	/**
	 * get all profiles
	 * @return
	 */
	@GetMapping("/equipo/all")
	public ResponseEntity<EquipoResponseRest> search(){
		 ResponseEntity<EquipoResponseRest> response = equipoService.search();
		 return response;
	}
	/**
	 * get profile by Id
	 * @param id
	 * @return
	 */
	@GetMapping("/equipo/{id}")
	public ResponseEntity<EquipoResponseRest> searchById(@PathVariable Long id){
		 ResponseEntity<EquipoResponseRest> response = equipoService.searchById(id);
		 return response;
	}
	/**
	 * save profile
	 * @param profile
	 * @return ResponseEntity<EquipoResponseRest> 
	 */
	@PostMapping("/equipo")
	public ResponseEntity<EquipoResponseRest> save(@RequestBody Equipo equipo){
		 ResponseEntity<EquipoResponseRest> response = equipoService.save(equipo);
		 return response;
	}
	/**
	 * update profile
	 * @param Perfil
	 * @param id
	 * @return  ResponseEntity<EquipoResponseRest>
	 */
	@PutMapping("/equipo/{id}")
	public ResponseEntity<EquipoResponseRest> update(@RequestBody Equipo equipo, @PathVariable Long id){
		 ResponseEntity<EquipoResponseRest> response = equipoService.update(equipo,id);
		 return response;
	}
	
	@DeleteMapping("/equipo/{id}")
	public ResponseEntity<EquipoResponseRest> delete(@PathVariable Long id){
		 ResponseEntity<EquipoResponseRest> response = equipoService.deleteById(id);
		 return response;
	}
}
