package com.consulti.fase3.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.consulti.fase3.model.Tarea;
import com.consulti.fase3.response.TareaResponseRest;
import com.consulti.fase3.services.TareaService;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1")
public class TareaController {
    @Autowired
    private TareaService tareaService;
	/**
	 * get all tareas
	 * @return
	 */
	@GetMapping("/tarea/all")
	public ResponseEntity<TareaResponseRest> search(){
		 ResponseEntity<TareaResponseRest> response = tareaService.search();
		 return response;
	}
	/**
	 * get tarea by Id
	 * @param id
	 * @return
	 */
	@GetMapping("/tarea/{id}")
	public ResponseEntity<TareaResponseRest> searchById(@PathVariable Long id){
		 ResponseEntity<TareaResponseRest> response = tareaService.searchById(id);
		 return response;
	}
	/**
	 * save profile
	 * @param profile
	 * @return ResponseEntity<TareaResponseRest> 
	 */
	@PostMapping("/tarea")
	public ResponseEntity<TareaResponseRest> save(
		@RequestParam("nombre" ) String nombre,
		@RequestParam("tiempo" ) int tiempo,
		@RequestParam("descripcion" ) String descripcion,
		 @RequestParam("lider") Long idLider) throws IOException{
		 Tarea tarea = new Tarea();
			tarea.setNombre(nombre);
			tarea.setTime(tiempo);
			tarea.setDescripcion(descripcion);
			tarea.setInicio(0);
			tarea.setInicio(0);
			ResponseEntity<TareaResponseRest> response = tareaService.save(tarea,idLider);
			return response;
		 
	}
	/**
	 * update profile
	 * @param Perfil
	 * @param id
	 * @return  ResponseEntity<TareaResponseRest>
	 */
	@PutMapping("/tarea/{id}")
	public ResponseEntity<TareaResponseRest> update(@RequestBody Tarea tarea, @PathVariable Long id){
		 ResponseEntity<TareaResponseRest> response = tareaService.update(tarea,id);
		 return response;
	}
	
	@DeleteMapping("/tarea/{id}")
	public ResponseEntity<TareaResponseRest> delete(@PathVariable Long id){
		 ResponseEntity<TareaResponseRest> response = tareaService.deleteById(id);
		 return response;
	}
}
