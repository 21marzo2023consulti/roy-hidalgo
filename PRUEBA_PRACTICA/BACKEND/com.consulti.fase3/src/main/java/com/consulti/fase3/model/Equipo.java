package com.consulti.fase3.model;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

import jakarta.persistence.Table;
@Entity
@Table(name = "equipo")
public class Equipo {
    private static final Long serialVersionUID = 1L;
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    private String nombres;
    private String identC;
    private int equipo;
    private int area;
    private int cargo;
	private List<Usuario> miembros = new ArrayList<>();
    public static Long getSerialversionuid() {
        return serialVersionUID;
    }
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getIdentC() {
        return identC;
    }
    public void setIdentC(String identC) {
        this.identC = identC;
    }
    public int getEquipo() {
        return equipo;
    }
    public void setEquipo(int equipo) {
        this.equipo = equipo;
    }
    public int getArea() {
        return area;
    }
    public void setArea(int area) {
        this.area = area;
    }
    public int getCargo() {
        return cargo;
    }
    public void setCargo(int cargo) {
        this.cargo = cargo;
    }
    public List<Usuario> getMiembros() {
        return miembros;
    }
    public void setMiembros(List<Usuario> miembros) {
        this.miembros = miembros;
    }
    public Equipo() {
    }
    public Equipo(Long id, String nombres, String identC, int equipo, int area, int cargo, List<Usuario> miembros) {
        Id = id;
        this.nombres = nombres;
        this.identC = identC;
        this.equipo = equipo;
        this.area = area;
        this.cargo = cargo;
        this.miembros = miembros;
    }
    @Override
    public String toString() {
        return "Equipo [Id=" + Id + ", nombres=" + nombres + ", identC=" + identC + ", equipo=" + equipo + ", area="
                + area + ", cargo=" + cargo + ", miembros=" + miembros + "]";
    }
    



}
