package com.consulti.fase3.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.consulti.fase3.model.Equipo;
@EnableJpaRepositories
@Repository
public interface EquipoRepo extends JpaRepository<Equipo,Long>{
    
}
