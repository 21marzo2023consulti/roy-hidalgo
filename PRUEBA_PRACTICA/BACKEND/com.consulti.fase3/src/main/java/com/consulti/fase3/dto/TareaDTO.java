package com.consulti.fase3.dto;

import java.util.ArrayList;
import java.util.List;

import com.consulti.fase3.model.Equipo;
import com.consulti.fase3.model.Usuario;

public class TareaDTO {
    private Long Id;
    private String nombre;
    private int area;
    private int time;
    private String descripcion;
    private int inicio;
    private int fin;
    private Usuario lider;
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getArea() {
        return area;
    }
    public void setArea(int area) {
        this.area = area;
    }
    public int getTime() {
        return time;
    }
    public void setTime(int time) {
        this.time = time;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public int getInicio() {
        return inicio;
    }
    public void setInicio(int inicio) {
        this.inicio = inicio;
    }
    public int getFin() {
        return fin;
    }
    public void setFin(int fin) {
        this.fin = fin;
    }
    public Usuario getLider() {
        return lider;
    }
    public void setLider(Usuario lider) {
        this.lider = lider;
    }
    public TareaDTO() {
    }
    public TareaDTO(Long id, String nombre, int area, int time, String descripcion, int inicio, int fin,
            Usuario lider) {
        Id = id;
        this.nombre = nombre;
        this.area = area;
        this.time = time;
        this.descripcion = descripcion;
        this.inicio = inicio;
        this.fin = fin;
        this.lider = lider;
    }
    @Override
    public String toString() {
        return "TareaDTO [Id=" + Id + ", nombre=" + nombre + ", area=" + area + ", time=" + time + ", descripcion="
                + descripcion + ", inicio=" + inicio + ", fin=" + fin + ", lider=" + lider + "]";
    }
    
    
}
