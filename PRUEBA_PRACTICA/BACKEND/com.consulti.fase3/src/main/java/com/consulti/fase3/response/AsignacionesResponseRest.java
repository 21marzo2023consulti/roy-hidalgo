package com.consulti.fase3.response;

public class AsignacionesResponseRest extends ResponseRest {
    private AsignacionesResponse asignacionesResponse = new AsignacionesResponse();

    public AsignacionesResponse getAsignacionesResponse() {
        return asignacionesResponse;
    }

    public void setAsignacionesResponse(AsignacionesResponse asignacionesResponse) {
        this.asignacionesResponse = asignacionesResponse;
    }
    
}
