package com.consulti.fase3.services.IMPL;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.consulti.fase3.model.Asignaciones;
import com.consulti.fase3.model.Usuario;
import com.consulti.fase3.repo.AsignacionesRepo;
import com.consulti.fase3.repo.UsuarioRepo;
import com.consulti.fase3.response.AsignacionesResponseRest;
import com.consulti.fase3.services.AsignacionesService;

@Service
public class AsignacionesIMPL  implements AsignacionesService{
    @Autowired
    private UsuarioRepo usuarioRepo;
    @Autowired
    private AsignacionesRepo asignacionesRepo;
    @Override
    public ResponseEntity<AsignacionesResponseRest> search() {
        AsignacionesResponseRest response = new AsignacionesResponseRest();
		try {
			List<Asignaciones> asignaciones = (List<Asignaciones>) asignacionesRepo.findAll();
			response.getAsignacionesResponse().setAsignaciones(asignaciones);
			response.setMetadata("Respuesta correcta", "00", "Respuesta exitosa");
		} catch (Exception e) {
			
			response.setMetadata("Respuesta incorrecta", "-1", "Error al consultar");
			e.getStackTrace();
			return new ResponseEntity<AsignacionesResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<AsignacionesResponseRest>(response,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<AsignacionesResponseRest> searchById(Long id) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'searchById'");
    }

    @Override
    public ResponseEntity<AsignacionesResponseRest> save(Asignaciones asignacion, Long idCoordinador, Long idOperativos) {
        AsignacionesResponseRest response = new AsignacionesResponseRest();
        List<Asignaciones> list = new ArrayList<>();

        try{
            Optional<Usuario> coordinador = usuarioRepo.findById(idCoordinador);
            Optional<Usuario> operativos = usuarioRepo.findById(idOperativos);
            System.out.println("Se encontro el coordinador: "+coordinador);
            System.out.println("Se encontro el opertivo: "+operativos);
            asignacion.setCoordinador(coordinador.get());
            asignacion.setOperativos(operativos.get());
            
            Asignaciones asignacionSaved = asignacionesRepo.save(asignacion);
            if(asignacionSaved != null){
                list.add(asignacionSaved);
                response.getAsignacionesResponse().setAsignaciones(list);
                response.setMetadata("respuesta ok", "00", "Asignacion guardada");
            } else {
                response.setMetadata("respuesta nok", "-1", "Asignacion no guardada");
                return new ResponseEntity<AsignacionesResponseRest>(response, HttpStatus.BAD_REQUEST);
            }
            
        }catch (Exception e) {
            e.getStackTrace();
            response.setMetadata("respuesta nok", "-1", "Error al guardar la asignacion");
            return new ResponseEntity<AsignacionesResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        return new ResponseEntity<AsignacionesResponseRest>(response, HttpStatus.OK);
    }
        /**
      * Generar el identificador
      */
     
/*      
     List<Asignaciones> list = new ArrayList<>();
     try {
         Asignaciones asignacionesSaved = asignacionesRepo.save(asignaciones);
         if(asignacionesSaved != null) {
             list.add(asignacionesSaved);
             response.getAsignacionesResponse().setAsignaciones(list);
             response.setMetadata("Respuesta correcta", "00", "Data guardada");
         } else {
             response.setMetadata("Respuesta icorrecta", "-1", "Data no guardado");
             return new ResponseEntity<AsignacionesResponseRest>(response,HttpStatus.BAD_REQUEST);
         }
         
     }
      catch (Exception e){
             response.setMetadata("Respuesta incorrecta", "-1", "Error al grabar uuario");
             e.getStackTrace();
             return new ResponseEntity<AsignacionesResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
         }
     return new ResponseEntity<AsignacionesResponseRest>(response,HttpStatus.OK);
    }
 */
    @Override
    public ResponseEntity<AsignacionesResponseRest> update(Asignaciones asignaciones, Long id) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

    @Override
    public ResponseEntity<AsignacionesResponseRest> deleteById(Long id) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'deleteById'");
    }
    
}
