package com.consulti.fase3.services;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.consulti.fase3.model.Empleado;
import com.consulti.fase3.response.EmpleadoResponseRest;

@Repository
public interface EmpleadoService {
    public ResponseEntity<EmpleadoResponseRest> search();
	public ResponseEntity<EmpleadoResponseRest> searchById(Long id);
	public ResponseEntity<EmpleadoResponseRest> save(Empleado empleado);
	public ResponseEntity<EmpleadoResponseRest> update(Empleado empleado, Long id);
	public ResponseEntity<EmpleadoResponseRest> deleteById(Long id);
}
