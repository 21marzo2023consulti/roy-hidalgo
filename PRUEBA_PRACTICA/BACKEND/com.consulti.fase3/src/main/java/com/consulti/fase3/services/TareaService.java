package com.consulti.fase3.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.consulti.fase3.model.Tarea;
import com.consulti.fase3.response.TareaResponseRest;

@Repository
public interface TareaService {
    public ResponseEntity<TareaResponseRest> search();
	public ResponseEntity<TareaResponseRest> searchById(Long id);
	public ResponseEntity<TareaResponseRest> save(Tarea tarea, Long idLider);
	public ResponseEntity<TareaResponseRest> update(Tarea tarea, Long id);
	public ResponseEntity<TareaResponseRest> deleteById(Long id);
}
