package com.consulti.fase3.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.consulti.fase3.model.Empleado;
import com.consulti.fase3.response.EmpleadoResponseRest;
import com.consulti.fase3.services.EmpleadoService;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1")
public class EmpleadoController {
    @Autowired
    private EmpleadoService empleadoService;
	/**
	 * get all profiles
	 * @return
	 */
	@GetMapping("/empleado/all")
	public ResponseEntity<EmpleadoResponseRest> search(){
		 ResponseEntity<EmpleadoResponseRest> response = empleadoService.search();
		 return response;
	}
	/**
	 * get profile by Id
	 * @param id
	 * @return
	 */
	@GetMapping("/empleado/{id}")
	public ResponseEntity<EmpleadoResponseRest> searchById(@PathVariable Long id){
		 ResponseEntity<EmpleadoResponseRest> response = empleadoService.searchById(id);
		 return response;
	}
	/**
	 * save profile
	 * @param profile
	 * @return ResponseEntity<EmpleadoResponseRest> 
	 */
	@PostMapping("/empleado")
	public ResponseEntity<EmpleadoResponseRest> save(@RequestBody Empleado empleado){
		 ResponseEntity<EmpleadoResponseRest> response = empleadoService.save(empleado);
		 return response;
	}
	/**
	 * update profile
	 * @param Perfil
	 * @param id
	 * @return  ResponseEntity<EmpleadoResponseRest>
	 */
	@PutMapping("/empleado/{id}")
	public ResponseEntity<EmpleadoResponseRest> update(@RequestBody Empleado empleado, @PathVariable Long id){
		 ResponseEntity<EmpleadoResponseRest> response = empleadoService.update(empleado,id);
		 return response;
	}
	
	@DeleteMapping("/empleado/{id}")
	public ResponseEntity<EmpleadoResponseRest> delete(@PathVariable Long id){
		 ResponseEntity<EmpleadoResponseRest> response = empleadoService.deleteById(id);
		 return response;
	}
    
}
