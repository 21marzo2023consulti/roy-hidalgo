package com.consulti.fase3.response;
import java.util.List;

import com.consulti.fase3.model.Equipo;
public class EquipoResponse {
    private List<Equipo> equipo;

    public List<Equipo> getEquipo() {
        return equipo;
    }

    public void setEquipo(List<Equipo> equipo) {
        this.equipo = equipo;
    }
    
}
