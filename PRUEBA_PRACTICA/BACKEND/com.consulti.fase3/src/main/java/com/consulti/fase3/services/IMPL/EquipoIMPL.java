package com.consulti.fase3.services.IMPL;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.consulti.fase3.model.Equipo;
import com.consulti.fase3.repo.EquipoRepo;
import com.consulti.fase3.response.EquipoResponseRest;
import com.consulti.fase3.services.EquipoService;
import jakarta.transaction.Transactional;

@Service
public class EquipoIMPL implements EquipoService{
    
    @Autowired
    private EquipoRepo equipoRepo;
    @Override
    public ResponseEntity<EquipoResponseRest> search() {
		EquipoResponseRest response = new EquipoResponseRest();
		try {
			List<Equipo> equipo = (List<Equipo>) equipoRepo.findAll();
			response.getEquipoResponse().setEquipo(equipo);;
			response.setMetadata("Respuesta correcta", "00", "Respuesta exitosa");
		} catch (Exception e) {
			
			response.setMetadata("Respuesta incorrecta", "-1", "Error al consultar");
			e.getStackTrace();
			return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.OK);
	}

    @Override
    @Transactional
    public ResponseEntity<EquipoResponseRest> searchById(Long id) {
        EquipoResponseRest response = new EquipoResponseRest();
		List<Equipo> list = new ArrayList<>();
		try {
			Optional<Equipo> data = equipoRepo.findById(id);
			if(data.isPresent()) {
				list.add(data.get());
				response.getEquipoResponse().setEquipo(list);
				response.setMetadata("Respuesta correcta", "00", "Usuario encontrado");
			} else {
				response.setMetadata("Respuesta incorrecta", "-1", "Usuario no encontrado");
				return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e){
			response.setMetadata("Respuesta incorrecta", "-1", "Error al consultar por id");
			e.getStackTrace();
			return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<EquipoResponseRest> save(Equipo equipo) {
        EquipoResponseRest response = new EquipoResponseRest();

        
		List<Equipo> list = new ArrayList<>();
		try {
			Equipo dataSaved = equipoRepo.save(equipo);
			if(dataSaved != null) {
				list.add(dataSaved);
				response.getEquipoResponse().setEquipo(list);
				response.setMetadata("Respuesta correcta", "00", "Data guardada");
			} else {
				response.setMetadata("Respuesta icorrecta", "-1", "Data no guardado");
				return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.BAD_REQUEST);
			}
			
		}
		 catch (Exception e){
				response.setMetadata("Respuesta incorrecta", "-1", "Error al grabar uuario");
				e.getStackTrace();
				return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<EquipoResponseRest> update(Equipo equipo, Long id) {
        EquipoResponseRest response = new EquipoResponseRest();
		List<Equipo> list = new ArrayList<>();
		try {
			Optional<Equipo> dataSearch = equipoRepo.findById(id);
			if(dataSearch.isPresent()) {
				//Save the register
			/* 	dataSearch.get().setUser(empleado.getNombres());
				dataSearch.get().setFecha(data.getFecha());
				dataSearch.get().setEntrada(data.getEntrada());
				dataSearch.get().setSalida(data.getSalida()); */
				
				Equipo dataToUpdate = equipoRepo.save(dataSearch.get());
				if(dataToUpdate != null) {
					list.add(dataToUpdate);
					response.getEquipoResponse().setEquipo(list);
					response.setMetadata("Respuesta correcta", "00", "Usuario actualizado");
					
				}
				
				
			} else {
				response.setMetadata("Respuesta incorrecta", "-1", " Usuario no actualizado");
				return new ResponseEntity<EquipoResponseRest>(response, HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e){
			response.setMetadata("Respuesta incorrecta", "-1", "Error al grabar usuario");
			e.getStackTrace();
			return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<EquipoResponseRest> deleteById(Long id) {
        EquipoResponseRest response = new EquipoResponseRest();
		try {
			equipoRepo.deleteById(id);
			response.setMetadata("Respuesta correcta", "00", "Registro eliminado");
		}catch (Exception e) {
			response.setMetadata("Respuesta incorecta", "-1","Error al eliminar");
			e.getStackTrace();
			return new ResponseEntity<EquipoResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<EquipoResponseRest>(response,HttpStatus.OK);
    }
        
}
