package com.consulti.fase3.response;

import java.util.List;

import com.consulti.fase3.model.Tarea;

public class TareaResponse {
    private List<Tarea> tarea;

    public List<Tarea> getTarea() {
        return tarea;
    }

    public void setTarea(List<Tarea> tarea) {
        this.tarea = tarea;
    }
    
}
