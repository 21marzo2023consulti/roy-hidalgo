package com.consulti.fase3.response;

public class TareaResponseRest extends ResponseRest{
    private TareaResponse tareaResponse = new TareaResponse();

    public TareaResponse getTareaResponse() {
        return tareaResponse;
    }

    public void setTareaResponse(TareaResponse tareaResponse) {
        this.tareaResponse = tareaResponse;
    }

 
    
}
