package com.consulti.fase3.services.IMPL;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.consulti.fase3.dto.LoginDTO;
import com.consulti.fase3.dto.UsuarioDTO;
import com.consulti.fase3.model.Usuario;
import com.consulti.fase3.repo.UsuarioRepo;
import com.consulti.fase3.response.LoginResponse;
import com.consulti.fase3.response.UsuarioResponseRest;
import com.consulti.fase3.services.UsuarioService;

@Service
public class UsuarioIMPL implements UsuarioService{
    @Autowired
    private UsuarioRepo usuarioRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public String registerUsuario(UsuarioDTO usuarioDTO) {
        
        /**
         * Generar el identificador
         */
        String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String numeros = "0123456789";
        StringBuilder cadenaAleatoria = new StringBuilder();
        Random random = new Random();
        
        // Generar 2 letras aleatorias
        for (int i = 0; i < 2; i++) {
            int index = random.nextInt(letras.length());
            char letra = letras.charAt(index);
            cadenaAleatoria.append(letra);
        }
        
        // Generar 2 números aleatorios
        for (int i = 0; i < 2; i++) {
            int index = random.nextInt(numeros.length());
            char numero = numeros.charAt(index);
            cadenaAleatoria.append(numero);
        }
        
        String cadena = cadenaAleatoria.toString();
        

        Usuario usuario = new Usuario(
            usuarioDTO.getId(),
            usuarioDTO.getNombres(),
            usuarioDTO.getApellidos(),
            usuarioDTO.getEmail(),
            this.passwordEncoder.encode(usuarioDTO.getPassword()),
            usuarioDTO.getFecha_nacimiento(),
            usuarioDTO.getArea(),
            cadena,
            usuarioDTO.getRol(), 
            usuarioDTO.getAsignaciones()
        );
        usuarioRepo.save(usuario);
        return usuario.getEmail();
    }
    @Override
    public ResponseEntity<UsuarioResponseRest> search() {
		UsuarioResponseRest response = new UsuarioResponseRest();
		try {
			List<Usuario> usuario = (List<Usuario>) usuarioRepo.findAll();
			response.getUsuarioResponse().setUsuario(usuario);
			response.setMetadata("Respuesta correcta", "00", "Respuesta exitosa");
		} catch (Exception e) {
			
			response.setMetadata("Respuesta incorrecta", "-1", "Error al consultar");
			e.getStackTrace();
			return new ResponseEntity<UsuarioResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<UsuarioResponseRest>(response,HttpStatus.OK);
	}
    @Override
    public LoginResponse loginUser(LoginDTO loginDTO) {
        System.out.println(loginDTO.getEmail());
        /**
         * Busco por email al usuario
         */
        Usuario userencontrado = usuarioRepo.findByEmail(loginDTO.getEmail());
        System.out.print(userencontrado);
        if(userencontrado != null){
            String password = loginDTO.getPassword();
            String encodedPassword = userencontrado.getPassword();
            /**
             * Verifico si las contrasenas coinciden
             */
            Boolean esCorrecta = passwordEncoder.matches(password, encodedPassword);
            if(esCorrecta){
                Optional<Usuario> user  =  usuarioRepo.findOneByEmailAndPassword(loginDTO.getEmail(),encodedPassword);
                if(user.isPresent()&& userencontrado.getRol()==1){
                    return new LoginResponse("Login correcto jefe",true,userencontrado.getId());
                } else if (user.isPresent()&& userencontrado.getRol()==2){
                    return new LoginResponse("Login correcto coordinador",true,userencontrado.getId());
                } else if (user.isPresent()&& userencontrado.getRol()==3){
                    return new LoginResponse("Login correcto operativo",true,userencontrado.getId());
                }  {
                    return new LoginResponse("Login fallo",false,null);
                }
            } else {
                return new LoginResponse("Password incorrecta", false,null);
            }
        } else {
            return new LoginResponse("El correo no existe", false,null);
        }
    }
    @Override
    public ResponseEntity<UsuarioResponseRest> update(Usuario usuario, Long id) {
        UsuarioResponseRest response = new UsuarioResponseRest();
        List<Usuario> list = new ArrayList<>();
        try{
         /**
          * Busco por Id al usuario que quiero actualizar
          */
         Optional<Usuario> usuarioSearch = usuarioRepo.findById(id);
         if(usuarioSearch.isPresent()){
             usuarioSearch.get().setNombres(usuario.getNombres());
             usuarioSearch.get().setApellidos(usuario.getApellidos());
             usuarioSearch.get().setEmail(usuario.getEmail());
             usuarioSearch.get().setFecha_nacimiento(usuario.getFecha_nacimiento());
             usuarioSearch.get().setArea(usuario.getArea());
             usuarioSearch.get().setRol(usuario.getRol());
             usuarioSearch.get().setPassword( this.passwordEncoder.encode(usuario.getPassword()));
             
             Usuario usuarioToUpdate = usuarioRepo.save(usuarioSearch.get());
             if(usuarioToUpdate != null){
                 list.add(usuarioToUpdate);
                 response.getUsuarioResponse().setUsuario(list);
                 response.setMetadata("Respuesta correcta", "00", "Usuario actualizado");
             }
         }else {
             response.setMetadata("Respuesta incorrecta", "-1", " Usuario no actualizado");
             return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.BAD_REQUEST);
         }
     }catch (Exception e){
         response.setMetadata("Respuesta incorrecta", "-1", "Error al actualizar usuario");
         e.getStackTrace();
         return new ResponseEntity<UsuarioResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
     }
         return new ResponseEntity<UsuarioResponseRest>(response,HttpStatus.OK);
    }
    @Override
    public ResponseEntity<UsuarioResponseRest> deleteById(Long id) {
       UsuarioResponseRest response = new UsuarioResponseRest();
       try{
        usuarioRepo.deleteById(id);
        response.setMetadata("Respuesta correcta", "00", "Registro eliminado");
       }catch (Exception e) {
		response.setMetadata("Respuesta incorecta", "-1","Error al eliminar");
		e.getStackTrace();
		return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	return new ResponseEntity<UsuarioResponseRest>(response,HttpStatus.OK);
    }
    
}
