package com.consulti.fase3.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.consulti.fase3.model.Usuario;

@EnableJpaRepositories
@Repository
public interface UsuarioRepo extends JpaRepository<Usuario, Long>{
    Usuario findByEmail(String email);
    Optional<Usuario> findOneByEmailAndPassword(String email, String encodedpassword);
}
