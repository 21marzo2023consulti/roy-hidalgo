package com.consulti.fase3.services;


import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import com.consulti.fase3.response.AsignacionesResponseRest;
import com.consulti.fase3.model.Asignaciones;


@Repository
public interface AsignacionesService {
    public ResponseEntity<AsignacionesResponseRest> search();
	public ResponseEntity<AsignacionesResponseRest> searchById(Long id);
	public ResponseEntity<AsignacionesResponseRest> save(Asignaciones asignacion, Long idCoordinador, Long idOperativos);
	public ResponseEntity<AsignacionesResponseRest> update(Asignaciones asignaciones, Long id);
	public ResponseEntity<AsignacionesResponseRest> deleteById(Long id);
}
