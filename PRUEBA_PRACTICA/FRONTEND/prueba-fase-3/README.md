## PASOS PARA LA EJECUCIÓN DEL FRONTEND (DOCKER):
##### Revisar que las variables de entorno tengan la siguiente estructura 
export const environment = {
  production: false,
  base_url: "http://localhost:8082/api/v1"
};

##### Crear la imagen de este app en docker
docker build -t angular .
##### Correr la imagen de Angular en la misma red que el backend en springboot y la base de datos en MySQL
docker run -d -it -p 80:80/tcp --name frontend --network red-consulti angular
##### A continuacion escriba solo < localhost > en el navegador y puede comenzar a utilizarl