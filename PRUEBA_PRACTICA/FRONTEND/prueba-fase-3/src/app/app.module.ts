import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/components/login/login.component';
import { SigninComponent } from './components/signin/components/signin/signin.component';
import { EmpleadosComponent } from './components/empleados/components/empleados/empleados.component';
import { CoordinadorComponent } from './components/coordinador/components/coordinador/coordinador.component';
import { OperarioComponent } from './components/operario/components/operario/operario.component';
import { AgregarempleadoComponent } from './components/agregarempleado/components/agregarempleado/agregarempleado.component';
import { AddempleadoComponent } from './components/addempleado/components/addempleado/addempleado.component';
import { AsignacionesComponent } from './components/asignaciones/components/asignaciones/asignaciones.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SigninComponent,
    EmpleadosComponent,
    CoordinadorComponent,
    OperarioComponent,
    AgregarempleadoComponent,
    AddempleadoComponent,
    AsignacionesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
