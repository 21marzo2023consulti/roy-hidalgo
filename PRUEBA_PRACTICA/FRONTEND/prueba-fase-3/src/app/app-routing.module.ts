import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/components/login/login.component';
import { SigninComponent } from './components/signin/components/signin/signin.component';
import { EmpleadosComponent } from './components/empleados/components/empleados/empleados.component';
import { CoordinadorComponent } from './components/coordinador/components/coordinador/coordinador.component';
import { OperarioComponent } from './components/operario/components/operario/operario.component';
import { AddempleadoComponent } from './components/addempleado/components/addempleado/addempleado.component';
import { AsignacionesComponent } from './components/asignaciones/components/asignaciones/asignaciones.component';

const routes: Routes = [
  {path:'', component:LoginComponent},
  {path:'sign-in',component:SigninComponent},
  {path:'jefe',component:EmpleadosComponent},
  {path:'coordinador/:id',component:CoordinadorComponent},
  {path:'operario/:id',component:OperarioComponent},
  {path:'addempleado',component:AddempleadoComponent},
  {path:'asignaciones',component:AsignacionesComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
