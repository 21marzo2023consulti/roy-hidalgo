import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from "src/environments/environment";

const base_url = environment.base_url;
@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {
  userToUpdate = {
    id:0,
    email:"",
    fecha_nacimiento:""
  }
  data={

  }
  usuarioDetails=null;
  usuarios: any[]=[];
  usuarioAsignar:any;
  usuariosArea:any[]=[];
  array:any[]=[];
  arra2:any[]=[];
  user:any[]=[];
  usuarios2: any[]=[];
  constructor(private http:HttpClient,private router:Router) { 

  }

  ngOnInit(): void {
    this.getUsuarios()
  }
  getUsuarios(){
    this.http.get(`${base_url}/usuario/all`).subscribe((data:any)=>{
      console.log(data);
      
      if(data.metadata[0].code=="00"){
        for(let i=0;i<data.usuarioResponse.usuario.length; i++){
          console.log(data.usuarioResponse.usuario[i].rol);
          if(data.usuarioResponse.usuario[i].rol==2){
            
            this.usuarios.push(data.usuarioResponse.usuario[i])
           this.usuarioDetails = data.usuarioResponse.usuario[i];
            console.log(this.usuarios);
          } else if(data.usuarioResponse.usuario[i].rol==3){
   
            this.usuarios2.push(data.usuarioResponse.usuario[i])
            
           this.usuarioDetails = data.usuarioResponse.usuario[i];
            console.log(this.usuarios);
          }
          
        }
     
        
        
      }
      console.log(this.usuarioDetails);
    })
 
  }
  updateUser(user:any, id:number){
    this.userToUpdate = user;
    console.log(this.userToUpdate);
  }
  updateUsuario(body:any,id:number){
    this.http.put(`${base_url}/usuario/${id}`,this.userToUpdate).subscribe((data:any)=>{
      console.log(data);
      
      if(data.metadata[0].code=="00"){
        for(let i=0;i<data.usuarioResponse.usuario.length; i++){
          console.log(data.usuarioResponse.usuario[i].rol);
          if(data.usuarioResponse.usuario[i].rol==2){
            
            this.usuarios.push(data.usuarioResponse.usuario[i])
           this.usuarioDetails = data.usuarioResponse.usuario[i];
            console.log(this.usuarios);
          } else if(data.usuarioResponse.usuario[i].rol==3){
   
            this.usuarios2.push(data.usuarioResponse.usuario[i])
            
           this.usuarioDetails = data.usuarioResponse.usuario[i];
            console.log(this.usuarios);
          }
          
        }
     
        
        
      }
      console.log(this.usuarioDetails);
      alert("Se edito el usario correctamente")
    })
  }
  deleteUsuario(id:number){
    this.http.delete(`${base_url}/usuario/${id}`).subscribe((data:any)=>{
      alert("Se elimino el registro")
    })
  }
  obtenerUser(user:any,id:number){
    this.user=user;
    console.log(user);
  }
  obtenerUserPorArea(user:any,area:number){
    this.usuarioAsignar=user;
    this.http.get(`${base_url}/usuario/all`).subscribe((data:any)=>{
      console.log(data);
      
      if(data.metadata[0].code=="00"){
        for(let i=0;i<data.usuarioResponse.usuario.length; i++){
          console.log(data.usuarioResponse.usuario[i].rol);
          if(data.usuarioResponse.usuario[i].rol==2&&data.usuarioResponse.usuario[i].area==area){
            
            this.usuariosArea.push(data.usuarioResponse.usuario[i])
           this.usuarioDetails = data.usuarioResponse.usuario[i];
            console.log(this.usuariosArea);
          } 
          
        }
     
        
        
      }
      console.log(this.usuarioDetails);
    })
  }
  borrarData(){
    this.usuariosArea=[];
  }
  //Asignar a la entidad 
  asignar(user:any){
  this.array.push(this.usuarioAsignar)
  this.arra2.push(user);
  console.log(user.id);
  const dataToUpload = new FormData;
    dataToUpload.append('coordinador',user.id);
    dataToUpload.append('operativos',this.usuarioAsignar.id);
    
    
    
    console.log(this.data);
    this.http.post(`${base_url}/asignaciones`,dataToUpload).subscribe((resp:any)=>{
      alert("Se asigno el usuario");
      this.router.navigateByUrl('/asignaciones');
    });
  }
}
