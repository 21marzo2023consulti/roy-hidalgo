import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
const base_url = environment.base_url;
@Component({
  selector: 'app-asignaciones',
  templateUrl: './asignaciones.component.html',
  styleUrls: ['./asignaciones.component.css']
})
export class AsignacionesComponent implements OnInit {

  asignaciones:any[]=[];
  coordinadores:any[]=[];
  operativos:any[]=[];
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit(): void {
    this.obtenerAsignaciones();
  }

  obtenerAsignaciones(){
    this.http.get(`${base_url}/asignaciones/all`).subscribe((data:any)=>{
      console.log(data);
    
      for(let i=0;i<data.asignacionesResponse.asignaciones.length;i++){
        this.asignaciones.push(data.asignacionesResponse.asignaciones[i])
        this.coordinadores.push(data.asignacionesResponse.asignaciones[i].coordinador)
        this.operativos.push(data.asignacionesResponse.asignaciones[i].operativos)
      }
      console.log("Las asignaciones son: ",this.asignaciones);
      console.log("Las coordinadores son: ",this.coordinadores);
      console.log("Las operativos son: ",this.operativos);
    
      
    })
  }
}
