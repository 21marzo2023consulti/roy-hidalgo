import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

const base_url = environment.base_url;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string = "";
  password: string = "";


  constructor(private router: Router, private http:HttpClient) { }

  ngOnInit(): void {
  }
  /**
   * Comienzo de la funcion para iniciar sesion
   */
   Login(){
    console.log(this.email);
    console.log(this.password);
    //Creo un objeto con la informacion que voy a mandar a la pagina de login
    let bodyData = {
      email:this.email,
      password:this.password
  };
  //Llamo al metodo post y envio la informacion guardada en el objeto anterior
  this.http.post(`${base_url}/usuario/login`,bodyData).subscribe((resultData:any)=>{
    console.log(resultData);
    //si el resultado del metodo post es El correo no existe mandar una alerta 
    if(resultData.message == "El correo no existe"){
      alert("Email no existe");
      //Si el resultado es Login correcto admin envia a la pagina del administrador
    } else if (resultData.message == "Login correcto jefe"){
      this.router.navigateByUrl(`/jefe`);
      //Si el resultado es Login correcto user envia a la pagina del usuario
    } else if(resultData.message == "Login correcto coordinador"){
      this.router.navigateByUrl(`/coordinador/${resultData.id}`,resultData.id);
    } else if(resultData.message == "Login correcto operativo"){
      
      this.router.navigateByUrl(`/operario/${resultData.id}`,resultData.id);
    } 
    //En caso ded que no sea ninguna de las condiciones envia Email o contrasena incorrectos
    else{
      alert("Email o contraseña incorrectos");
    }
  })

  }

}
