import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

const base_url = environment.base_url;
@Component({
  selector: 'app-coordinador',
  templateUrl: './coordinador.component.html',
  styleUrls: ['./coordinador.component.css']
})
export class CoordinadorComponent implements OnInit {
  tarea={
    nombre:"",
    time:"",
    descripcion:""
  };
  array:any[]=[];
  id:string="";
 tareas:any[]=[]
 asignaciones:any[]=[];
 usuarioArea:any[]=[];
 inicio:string[]=[];
 final:string[]=[];
  constructor(private http:HttpClient,private route:ActivatedRoute) { 
    console.log(this.route.snapshot.paramMap.get('id'));
    this.id = this.route.snapshot.paramMap.get('id')!;
  }

  ngOnInit(): void {
    this.getTarea();
    this.getAsignaciones();
  }
  getTarea(){
    this.http.get(`${base_url}/tarea/all`).subscribe((data:any)=>{
      console.log(data);
      
      if(data.metadata[0].code=="00"){
        for(let i=0;i<data.tareaResponse.tarea.length; i++){

          console.log("El id del lider es: ",data.tareaResponse.tarea[i].lider.id)
    this.tareas.push(data.tareaResponse.tarea[i])
            if(this.tareas[i].inicio==1){
              this.tareas[i].inicio="SI"
            }else{
              this.tareas[i].inicio="NO"
            }
            if(this.tareas[i].fin==1){
              this.tareas[i].fin="SI"
            }else{
              this.tareas[i].fin="NO"
            }
 
            

        }
      }
    })
 
  }
  getAsignaciones(){
    this.http.get(`${base_url}/asignaciones/all`).subscribe((data:any)=>{
      console.log(data);
      for(let i =0; i < data.asignacionesResponse.asignaciones.length;i++){
        if(data.asignacionesResponse.asignaciones[i].coordinador.id==this.id){
           console.log(data.asignacionesResponse.asignaciones[i].coordinador);
        }
      }
      
    })
  }
  crearTarea(){
    console.log(this.tarea);
    this.http.post(`${base_url}/tarea`,this.tarea).subscribe((data:any)=>{
      alert("Tarea creada")
    })
  }

  verTarea(){

    this.http.get(`${base_url}/asignaciones/all`).subscribe((data:any)=>{
      console.log(data);
      
      if(data.metadata[0].code=="00"){
        for(let i=0;i<data.asignacionesResponse.asignaciones.length; i++){
          console.log(data.asignacionesResponse.asignaciones[i].operativos);
          if(data.asignacionesResponse.asignaciones[i].coordinador.id==this.id){
            
            this.usuarioArea.push(data.asignacionesResponse.asignaciones[i].operativos)
           
            console.log(this.usuarioArea);
          } 
          
        }
     
        
        
      }
      
    })
    
  }
  borrarData(){
    this.usuarioArea=[];
  }
  //Asignar a la entidad 
  asignar(user:any){
    this.array.push(user)

    const dataToUpload = new FormData;
      dataToUpload.append('lider',user.id);
      dataToUpload.append('nombre',this.tarea.nombre);
      dataToUpload.append('tiempo',this.tarea.time);
      dataToUpload.append('descripcion',this.tarea.descripcion);
      dataToUpload.append('inicio',"0");
      dataToUpload.append('fin',"0");
      
      
      
      this.http.post(`${base_url}/tarea`,dataToUpload).subscribe((resp:any)=>{
        alert("Se asigno la tarea");
      });
    }
}
