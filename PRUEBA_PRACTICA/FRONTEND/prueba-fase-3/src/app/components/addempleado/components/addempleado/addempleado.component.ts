import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
const base_url = environment.base_url;
@Component({
  selector: 'app-addempleado',
  templateUrl: './addempleado.component.html',
  styleUrls: ['./addempleado.component.css']
})
export class AddempleadoComponent implements OnInit {
  nivel:number=0;
  area:number=0;
  pass:string="";
  rol:number=0;
  motivos:number=0;
  passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@&_\-\$%\^&\*])([A-Za-z\d@&_\-\$%\^&\*]{8,})$/;

  registerForm = new FormGroup({
    nombres: new FormControl('', [Validators.required]),
    apellidos: new FormControl('', [Validators.required]),
    fecha_nacimiento: new FormControl('',[Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@&_\-\$%\^&\*])([A-Za-z\d@&_\-\$%\^&\*]{8,})$/)]),
    especialidad: new FormControl('', [Validators.required]),
    area: new FormControl(0, [Validators.required, Validators.min(1)]),
    rol: new FormControl(0, [Validators.required, Validators.min(1)]),
    nivel: new FormControl(0, [Validators.required, Validators.min(1)]),
    motivos: new FormControl(0, [Validators.required, Validators.min(1)])
  });
  
  checkboxesUser = [
 
    { id: 2, value: 'Coordinador', disabled: false },
    { id: 3, value: 'Operativo', disabled: false },
  ];
  
  checkboxesArea = [
    { id: 1, value: 'Tecnica', disabled: false },
    { id: 2, value: 'Comercial', disabled: false },
    { id: 3, value: 'Financiera', disabled: false },
    { id: 4, value: 'Soporte', disabled: false },
  ];
  
  constructor(private router:Router, private http:HttpClient) { }

  ngOnInit(): void {
  }
  /**
   * Funcion para los checkboxes
   * 
   */
  onChangeU(event:any){
    if(event.target.checked){
      this.checkboxesUser.forEach((checkbox)=>{
        if(checkbox.id != event.target.id){
          checkbox.disabled=true;
          
    
        }
        else{
          console.log("seleccionado el  Usuario "+checkbox.value)
    
    
          this.rol = checkbox.id;
        }
      })
    }
    else{
      this.checkboxesUser.forEach((checkbox)=>{
        checkbox.disabled = false;
      })
    }
  }
  onChangeA(event:any){
    console.log("lo toco");
    if(event.target.checked){
      this.checkboxesArea.forEach((checkbox)=>{
        if(checkbox.id != event.target.id){
          checkbox.disabled=true;
      
        }
        else{
          console.log("seleccionado el Area "+checkbox.value)
          this.area = checkbox.id;
        }
      })
    }
    else{
      this.checkboxesArea.forEach((checkbox)=>{
        checkbox.disabled = false;
      })
    }
  }

  /**
   * Funcion para registrar al usuario
   */

  save(){
    let password = this.registerForm.get('password')?.value;
    this.pass=password;
    //Covertir a fecha la fecha de nacimiento
    const fecha_nacimiento = new Date (this.registerForm.get('fecha_nacimiento')?.value);
    //Calcualr la fecha actual
    const fechaActual = new Date();
    //Calcular la edad
    const edad = fechaActual.getFullYear() - fecha_nacimiento.getFullYear();
    if(edad <18){
      alert("Debe ser mayor de edad para registrarse");

    }
    else{
      console.log(this.pass);
    if(this.pass.length>10){
      alert('El password no cumple los campos requeridos')
    }
    else if(this.pass.includes("@")||this.pass.includes("_")) {
      
      let bodyData={
        "nombres":this.registerForm.get('nombres')?.value,
        "apellidos":this.registerForm.get('apellidos')?.value,
        "fecha_nacimiento":this.registerForm.get('fecha_nacimiento')?.value,
        "email":this.registerForm.get('email')?.value,
        "password":this.registerForm.get('password')?.value,
        "area":this.area,
        "identificador":"",
        "rol":this.rol,

      };
      console.log(bodyData);
      this.http.post(`${base_url}/usuario/register`,bodyData,{responseType:'text'}).subscribe((resultData:any)=>{
        console.log(resultData);
        alert("Usuario registrado exitosamente");
        this.router.navigateByUrl('/jefe');
      })
    }
    else{
      alert("El campo no contiene los solicitado")
    }
  
    }


  }

}
