import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

const base_url = environment.base_url;
@Component({
  selector: 'app-operario',
  templateUrl: './operario.component.html',
  styleUrls: ['./operario.component.css']
})
export class OperarioComponent implements OnInit {
  id:string="";
  tareas:any[]=[];
  tarea={
    id:0,
    nombre:"",
    time:0,
    descripcion:"",
    inicio:0,
    fin:0,
    lider:null

  };
  constructor(private http:HttpClient,private route:ActivatedRoute) { 
    console.log(this.route.snapshot.paramMap.get('id'));
    this.id = this.route.snapshot.paramMap.get('id')!;
  }

  ngOnInit(): void {
    this.getLider();
  }
  getLider(){
    this.http.get(`${base_url}/tarea/all`).subscribe((data:any)=>{
      console.log(data);
      for(let i =0; i < data.tareaResponse.tarea.length;i++){
        if(data.tareaResponse.tarea[i].lider.id==this.id){
           console.log(data.tareaResponse.tarea[i].lider);
           this.tareas.push(data.tareaResponse.tarea[i]);
        }
      }
      
    })
  }
  iniciar(tarea:any){
    this.tarea=tarea;
    this.tarea.inicio=1;
    console.log(this.tarea)
    this.http.put(`${base_url}/tarea/${tarea.id}`,this.tarea).subscribe((data:any)=>{
      alert("La tarea inicio")
    })
  }
  terminar(tarea:any){
    tarea.fin=1
    console.log(tarea)
    this.http.put(`${base_url}/tarea/${tarea.id}`,tarea).subscribe((data:any)=>{
      alert("La tarea inicio")
    })
  }
}
