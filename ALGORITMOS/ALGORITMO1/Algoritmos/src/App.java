import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
         // Asociación de números con sus respectivas marcas
         Map<Integer, String> marcas = new HashMap<>();
         marcas.put(1, "Nike");
         marcas.put(2, "Adidas");
         marcas.put(3, "Puma");
         marcas.put(4, "Reebok");
         marcas.put(5, "Under Armour");
         marcas.put(6, "Bunky");
         marcas.put(7, "Venus");
         marcas.put(8, "Vans");
         marcas.put(9, "Fila");
         marcas.put(10, "Converse");
 
         // Pedir al usuario que ingrese los números separados por comas
         Scanner sc = new Scanner(System.in);
         System.out.print("Ingrese hasta 5 números separados por comas: ");
         String input = sc.nextLine();
 
         
         //Compruebo que no haya utilizado ni espacios, ni slash, ni guion para separa los numeros
         if(input.contains(" ")||input.contains("/")||input.contains("-")){
            System.out.println("El formato no es permitido");
             return;
         }
         else{
            //Si cumple la condicion divide el string separando por las comas y lo guarda en un arreglo
            String[] numeros = input.split(",");
            //Comparo su la entrada de los numeros tiene 0 o mas de 4 digitos
            if (numeros.length == 0 || numeros.length > 5) {
                System.out.println("El formato no es permitido");
                return;
            }
    
            // Verificar que no haya más de 2 repeticiones de cada número
            Map<Integer, Integer> repeticiones = new HashMap<>();
            for (String numero : numeros) {
                int n = Integer.parseInt(numero.trim());
                //Cuento cuantas repeticiones tiene cada valor
                int count = repeticiones.getOrDefault(n, 0);
                if (count >= 2) {
                    System.out.println("La marca " + marcas.get(n) + " se encuentra repetida " + (count+1) + " veces");
                    return;
                }
                repeticiones.put(n, count + 1);
            }
            System.out.println(repeticiones);
    
            // Construir la cadena final
            StringBuilder sb = new StringBuilder();
            for (String numero : numeros) {
                int n = Integer.parseInt(numero.trim());
                String marca = marcas.get(n);
                int opuesto = 11 - n;
                int count = repeticiones.get(n);
                if (count == 1) {
                    sb.append(marca).append("|"+marca.repeat(opuesto));
                } else {
                    sb.append(marca).append("|".repeat(opuesto * count));
                }
                repeticiones.put(n, count - 1);
            }
    
            // Imprimir la cadena final
            String cadenaFinal = sb.toString();
            System.out.println(cadenaFinal);
         }
       
     
    }
}
